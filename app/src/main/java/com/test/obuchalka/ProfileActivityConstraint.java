package com.test.obuchalka;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import static com.test.obuchalka.ProfleActivityDefault.setSpanToString;

public class ProfileActivityConstraint extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        setContentView(R.layout.activity_profile_constraint);
        String str = getString(R.string.posts);
        TextView textView = (TextView) findViewById(R.id.num_posts);
        setSpanToString(this, textView, str);
        str = getString(R.string.followers);
        textView = (TextView) findViewById(R.id.num_followers);
        setSpanToString(this, textView, str);
        str = getString(R.string.following);
        textView = (TextView) findViewById(R.id.num_following);
        setSpanToString(this, textView, str);
    }


}
