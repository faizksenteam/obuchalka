package com.test.obuchalka;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TextAppearanceSpan;
import android.view.WindowManager;
import android.widget.TextView;

public class ProfleActivityDefault extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        setContentView(R.layout.activity_profile_main_default);
        String str = getString(R.string.posts);
        TextView textView = (TextView) findViewById(R.id.num_posts);
        setSpanToString(this, textView, str);
        str = getString(R.string.followers);
        textView = (TextView) findViewById(R.id.num_followers);
        setSpanToString(this, textView, str);
        str = getString(R.string.following);
        textView = (TextView) findViewById(R.id.num_following);
        setSpanToString(this, textView, str);
    }

    public static void setSpanToString(Context context, TextView textView, String string) {
        SpannableString spannableString = new SpannableString(string);
        int start = string.indexOf('\n'), end = string.length();
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.blue_grey)),
                start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new TextAppearanceSpan(context,R.style.RegularFontTextView), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(0.875f),start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
    }
}
